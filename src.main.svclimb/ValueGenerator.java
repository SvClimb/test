import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class ValueGenerator {

    public static String generateNum(List<Integer> numbers) {
        if (numbers == null || numbers.isEmpty()) {
            return getString(1);
        }


        if (numbers.get(numbers.size() - 1) == numbers.size()) {
            var lastNumber = numbers.get(numbers.size() - 1) + 1;
            return getString(lastNumber);
        }

        List<Integer> intSet = IntStream.iterate(1, i -> i + 1)
                .limit(numbers.size())
                .boxed()
                .collect(toList());

        for (int i = 0; i < intSet.size(); i++) {
            if (!numbers.get(i).equals(intSet.get(i))) {
                return getString(intSet.get(i));
            }
        }
        return getString(1);
    }

    private static String getString(Integer lastNumber) {
        StringBuilder sb = new StringBuilder();
        if (lastNumber > 99) {
            return lastNumber.toString();
        } else if (lastNumber > 9) {
            return sb.append("0").append(lastNumber).toString();
        } else {
            return sb.append("00").append(lastNumber).toString();
        }
    }

    public static void main(String[] args) {

    }
}
